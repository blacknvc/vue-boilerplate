import axios from 'axios'
import TokenService from '../services/storage.service'
import OauthService from '../services/oauth.service'

const ApiService = {

    init(baseURL) {
        axios.defaults.baseURL = baseURL;
    },

    setHeader() {
        axios.defaults.headers.common["Authorization"] = `Bearer ${TokenService.getToken()}`
        axios.defaults.headers.common["Content-Type"] = 'application/json'
    },

    removeHeader() {
        axios.defaults.headers.common = {}
    },

    setHeaderOauth(clientId, clientSecret){
        axios.defaults.headers.common["Authorization"] = `Basic ${clientId}:${clientSecret}`
        axios.defaults.headers.common["Content-Type"] = 'application/x-www-form-urlencoded'
        axios.defaults.headers.common["Access-Control-Allow-Origin"] = "*"
    },

    get(resource) {
        return axios.get(resource)
    },

    post(resource, data) {
        return axios.post(resource, data)
    },

    put(resource, data) {
        return axios.put(resource, data)
    },

    delete(resource) {
        return axios.delete(resource)
    },

    loginRequest(data){
        if(OauthService.validateToken()){
            return axios(data);
        }else{
            OauthService.requestToken()
            return axios(data);
        }
    },
    /**
     * Perform a custom Axios request.
     *
     * data is an object containing the following properties:
     *  - method
     *  - url
     *  - data ... request payload
     *  - auth (optional)
     *    - username
     *    - password
    **/
    customRequest(data) {
        return axios(data)
    }
}

export default ApiService
