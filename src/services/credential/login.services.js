import ApiService from '../api.service'
import TokenService from '../storage.service'


class AuthenticationError extends Error {
    constructor(errorCode, message) {
        super(message)
        this.name = this.constructor.name
        this.message = message
        this.errorCode = errorCode
    }
}

const LoginServices = {
    /**
     * Login the user and store the access token to TokenService.
     *
     * @returns access_token
     * @throws AuthenticationError
    **/
    login: async function(username, password) {
        const requestData = {
            method: 'post',
            url: "/login",
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer '+TokenService.getToken()
            },
            data: {
                username: username,
                password: password
            }
        }

        try {
            const response = await ApiService.loginRequest(requestData);

            // response.data.access_token = response.data.refresh_token = response.data.token //remove after testing
            // TokenService.saveToken(response.data.access_token)
            // TokenService.saveRefreshToken(response.data.refresh_token)
            // ApiService.setHeader()

            // NOTE: We haven't covered this yet in our ApiService
            //       but don't worry about this just yet - I'll come back to it later
            //ApiService.mount401Interceptor();
            return response.data
        } catch (error) {
            throw new AuthenticationError(error.response.status, error.response.data.detail)
        }
    },

    /**
     * Logout the current user by removing the token from storage.
     *
     * Will also remove `Authorization Bearer <token>` header from future requests.
    **/
    logout() {
        // Remove the token and remove Authorization header from Api Service as well
        TokenService.removeToken()
        TokenService.removeRefreshToken()
        ApiService.removeHeader()

        // NOTE: Again, we'll cover the 401 Interceptor a bit later.
        //ApiService.unmount401Interceptor()
    }
}

export default LoginServices

export { LoginServices, AuthenticationError }
