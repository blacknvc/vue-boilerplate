import ApiService from './api.service'
import TokenService from './storage.service'
import md5 from 'md5'
import moment from 'moment'
import qs from 'qs'


class AuthenticationError extends Error {
    constructor(errorCode, message) {
        super(message)
        this.name = this.constructor.name
        this.message = message
        this.errorCode = errorCode
    }
}

function hashData(data){
    let time = moment.utc(moment()).format("YYYY_HH_MM_DD");

    return md5(data + time).toLowerCase();
}

function hashDataSecret(data) {
    var time = moment.utc(moment()).format('YYYY_HH_MM_DD');

    return md5(time+data).toLowerCase();
}

const OauthService = {

     testingToken : async function (){
       let token = TokenService.getToken();

         const requestData = {
             method: 'POST',
             url: "/test/connection",
             headers: {
                 'Authorization' : 'Bearer '+token,
                 'Content-Type': 'application/json',
             },
             data: {}
         }

         try{
             const response = await ApiService.customRequest(requestData)
             return response.data
         }catch (e) {
             return e;
         }
     },

    /**
     * Requesting access token and store the access token to
     * TokenService when app was mounted.
     *
     * @returns access_token
     * @throws AuthenticationError
     **/

     requestToken : async function  (){

        let hashAuthData = {
            client_id: process.env.VUE_APP_CLIENT_ID,//hashData(process.env.VUE_APP_CLIENT_ID),
            client_secret: process.env.VUE_APP_CLIENT_SECRET,//hashDataSecret(process.env.VUE_APP_CLIENT_SECRET),
            grant_type : 'client_credentials'
        }

        const requestData = {
            method: 'POST',
            url: "/oauth/token",
            headers: {
              'Authorization' : 'Basic '+new Buffer(hashAuthData.client_id.toLowerCase() +
                  ":" + hashAuthData.client_secret.toLowerCase()).toString("base64"),
              'Content-Type': 'application/x-www-form-urlencoded',
            },
            data: qs.stringify(hashAuthData)
        }

        try {

            const response = await ApiService.customRequest(requestData)
            TokenService.saveToken(response.data.data.access_token)
            TokenService.saveRefreshToken(response.data.data.refresh_token)
            TokenService.saveExpireToken(response.data.data.expires_in)


            // NOTE: We haven't covered this yet in our ApiService
            //       but don't worry about this just yet - I'll come back to it later
           // ApiService.mount401Interceptor();

            return false//response.data.access_token
        } catch (error) {
            throw new AuthenticationError(error.response.status, error.response.data.detail)
        }
    },

    validateToken(){
        let expTimeToken = TokenService.getExpireToken()
        let timeNow =  moment().utc().format()
        let compare = moment(timeNow).isBefore(expTimeToken)

        console.log("TIME EXP", expTimeToken)
        console.log("TIME NOW", timeNow)
        console.log("EXP RES => ", compare)

        //If True Forward The Request
        if(compare){
            return true
        }else{
            this.requestToken()
        }
    },
}


export default OauthService
