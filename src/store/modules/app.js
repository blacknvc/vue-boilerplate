import OauthService from '../../services/oauth.service'
import TokenService from '../../services/storage.service'

const state =  {
   // accessToken: TokenService.getToken(),
}

const getters = {
    // tokenIsSet: (state) => {
    //     return state.accessToken ? true : false
    // },
    //
    // authenticating: (state) => {
    //     return state.authenticating
    // }
}

const actions = {
    async doAuth({ commit }) {

        try {
            const token = await OauthService.requestToken();


            // Redirect the user to the page he first tried to visit or to the home view
            //router.push(router.history.current.query.redirect || '/my-profile');
            return true
        } catch (e) {
            // if (e instanceof AuthenticationError) {
            //     commit('loginError', {errorCode: e.errorCode, errorMessage: e.message})
            // }
            return false
        }
    },

}

const mutations = {
    // oauthRequest(state) {
    //     state.authenticating = true;
    // },
    //
    // oauthSuccess(state, accessToken) {
    //     state.accessToken = accessToken
    //     state.authenticationSuccess = true;
    //     state.authenticating = false;
    // },
    //
    // loginError(state, {errorCode, errorMessage}) {
    //     state.authenticating = false
    //     state.authenticationErrorCode = errorCode
    //     state.authenticationError = errorMessage
    // },
    //
    // logoutSuccess(state) {
    //     state.accessToken = ''
    // }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
