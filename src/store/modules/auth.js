import { LoginServices, AuthenticationError } from '../../services/credential/login.services'
import TokenService from '../../services/storage.service'
import router from '../../router'
import OauthService from "../../services/oauth.service"
import moment from 'moment'


const state =  {
    authenticating: false,
    loggedIn : false,
    authenticationSuccess: false,
    authenticationErrorCode: 0,
    authenticationError: ''
}

const getters = {
    loggedIn: (state) => {
        return state.loggedIn
    },

    authenticationErrorCode: (state) => {
        return state.authenticationErrorCode
    },

    authenticationError(state){
        return state.authenticationError
    },

    authenticationSuccess(state){
        return state.authenticationSuccess
    },

    authenticating: (state) => {
        return state.authenticating
    }
}

const actions = {
    async login({ commit }, {email, password}) {
        commit('loginRequest');
        try {
            const response = await LoginServices.login(email, password);
            console.log("response", response)
            if(response.data.code != '99'){
                commit('loginSuccess', response.data.data)
                // Redirect the user to the page he first tried to visit or to the home view
                router.push(router.history.current.query.redirect || '/my-profile');
                return true
            }else{
                commit('loginError',  {errorCode: response.code, errorMessage: response.message})
                return false
            }
        } catch (e) {
            if (e instanceof AuthenticationError) {
                commit('loginError', {errorCode: e.errorCode, errorMessage: e.message})
            }
            return false
        }
    },
    logout({ commit }) {
        LoginServices.logout()
        commit('logoutSuccess')
        router.push('/login')
    }
}

const mutations = {
    loginRequest(state) {
        state.authenticating = true;
        state.authenticationError = ''
        state.authenticationErrorCode = 0
    },

    loginSuccess(state) {
        state.loggedIn = true;
        state.authenticationSuccess = true;
        state.authenticating = false;
    },

    loginError(state, {errorCode, errorMessage}) {
        state.authenticating = false
        state.authenticationErrorCode = errorCode
        state.authenticationError = errorMessage
    },

    logoutSuccess(state) {
        state.loggedIn = false
    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
